function getUrl() {
	return process.env.REACT_APP_IMAGES_API_URL+'/image/';
}

function getImages() {
	return fetch(getUrl())
	.then(response => response.json())
	;
};

export {
	getUrl,
	getImages,
};
