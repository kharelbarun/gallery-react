export default
function Item({ image, onOpenPreview }) {

	return (
		<div className="-image-list-item">
			<a href="#"
				onClick={(ev)=>{ev.preventDefault();onOpenPreview();}}
				aria-label={image.caption}
			>
				<img
					src={image.url}
					alt={image.altText}
					title={image.titleText}
				/>
			</a>
		</div>
	);
}
