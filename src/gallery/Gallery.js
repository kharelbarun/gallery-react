import React from "react";
import Item from "./Item";
import Preview from "./Preview";

class Gallery extends React.Component {

	constructor(props) {
		super(props);
		this.previewClosedState = {
			previewIndex: -1,
			previewImage: null,
			previewHasPrevious: false,
			previewHasNext: false,
		};
		this.state = {
			...this.previewClosedState,
		};
	}

	render() {
		return (
			<div className="container gallery">
				<div className="-image-list">
					{this.props.images.map((image, index) => (
						<Item key={index}
							image={image}
							onOpenPreview={()=>{this.openPreview(index)}}
						/>
					))}
				</div>
				{this.state.previewImage && (
					<Preview
						image={this.state.previewImage}
						hasPrevious={this.state.previewHasPrevious}
						hasNext={this.state.previewHasNext}
						onShowPrevious={()=>{this.previewPrevious()}}
						onShowNext={()=>{this.previewNext()}}
						onClosePreview={()=>{this.closePreview()}}
					/>
				)}
			</div>
		);
	}

	openPreview(index) {
		this.updatePreview(index);
	}

	updatePreview(previewIndex) {
		this.setState({
			previewIndex: previewIndex,
			previewImage: this.props.images[previewIndex],
			previewHasPrevious: previewIndex > 0,
			previewHasNext: previewIndex < this.props.images.length - 1,
		});
	}

	previewPrevious() {
		if (this.state.previewIndex > 0) {
			this.updatePreview(this.state.previewIndex-1);
		}
	}

	previewNext() {
		if (this.state.previewIndex < this.props.images.length - 1) {
			this.updatePreview(this.state.previewIndex+1);
		}
	}

	closePreview() {
		this.setState({
			...this.previewClosedState,
		});
	}
}

export default Gallery;
