import React from "react";

class Preview extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			hintIsOpen: false,
		};
		this.previewElementRef = React.createRef();
	}

	render() {
		return (
			<div className="-preview"
				tabIndex="-1"
				onKeyDown={ev=>this.onKeydown(ev.key)}
				ref={this.previewElementRef}
			>
				<div className="-box">
					<div className="-content">
						<button className="-close" title="Close"
							onClick={()=>this.props.onClosePreview()}
						></button>
						<button title="Previous"
							className={[
								'-control -left',
								this.props.hasPrevious ? '' : 'disabled',
							].join(' ')}
							onClick={() => this.props.onShowPrevious()}
						></button>
						<div className="-image-caption-container">
							<div className="-image">
								<img src={this.props.image.url}
									alt={this.props.image.altText}
									title={this.props.image.titleText}
								/>
							</div>
							<div className="-caption">
								<span className="-text">{this.props.image.caption}</span>
							</div>
						</div>
						<button title="Next"
							className={[
								'-control -right',
								this.props.hasNext ? '' : 'disabled',
							].join(' ')}
							onClick={()=>this.props.onShowNext()}
						></button>
					</div>
				</div>
				<div className="-hint">
					<button title="Hint" className="-icon-wrapper"
						onClick={()=>this.openHint()}
					>
						<span className="-icon"></span>
					</button>
					<div className="-text-wrapper"
						hidden={!this.state.hintIsOpen}
					>
						<div className="-text">
							<b>Hint:</b>
							<span> </span>
							<span>
								Use <kbd>Left</kbd>, <kbd>Right</kbd> and <kbd>Esc</kbd> keys
								to show previous, show next and close preview respectively. 
							</span>
						</div>
					</div>
				</div>
			</div>
		);
	}

	componentDidMount() {
		document.body.style.overflow = 'hidden';
		this.previewElementRef.current.focus();
	}

	componentWillUnmount() {
		document.body.style.removeProperty('overflow');
		this.previewElementRef.current.blur();
	}

	onKeydown(key) {
		switch (key) {
			case 'ArrowLeft':
				this.props.onShowPrevious();
				break;
			case 'ArrowRight':
				this.props.onShowNext();
				break;
			case 'Escape':
				this.props.onClosePreview();
				break;
			default:
		}
	}

	openHint() {
		this.setState({
			hintIsOpen: !this.state.hintIsOpen,
		});
	}
}

export default Preview;
