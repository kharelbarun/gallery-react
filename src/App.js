import logo from './logo.svg';
import './App.css';
import Gallery from './gallery/Gallery';
import * as imagesService from './imagesService';
import { useState, useEffect, useRef } from 'react';

function App() {

	let [images, setImages] = useState([]);
	const dataFetchedRef = useRef(false);

	useEffect(() => {
		if (dataFetchedRef.current) {
			return;
		}
		dataFetchedRef.current = true;
		imagesService.getImages().then(setImages);
	}, []);

	return (
		<div className="App">

			<div className="section-wrapper">

				<div className="toolbar">
					<div className="container">
						<h1 className="heading">Gallery</h1>
					</div>
				</div>

				<div className="section-content">
					<div className="container">
						<p><b>Gallery</b> is a React component made to showcase images.</p>
					</div>
					<Gallery images={images}/>
				</div>

				<footer className="footer">
					<div className="container text-center">
						Made with React
						<span> </span>
						<img src={logo} className="react-logo" alt="logo" />
					</div>
				</footer>

			</div>

		</div>
	);
}

export default App;
